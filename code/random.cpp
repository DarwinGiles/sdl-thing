#include "random.h"

bool randBool()
{
    return (rand() % 2) == 1;
}

float randPct()
{
    return rand() * 1.0 / RAND_MAX;
}

void seedRNG()
{
    time_t now;
    time(&now);
    srand(now);
}