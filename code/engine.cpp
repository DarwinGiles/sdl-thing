#include "engine.h"

static SDL_Renderer *renderer;
static SDL_Texture *texture;
static void *pixels;

void drawRects(World *world)
{
    uint32 entity;

    for (entity = 0; entity < ENTITY_COUNT; entity++)
    {
        if (world->mask[entity] & COMPONENT_RECT)
        {
            Rect *rect = &world->rect[entity];

            uint8 *row = (uint8 *) pixels;
            row += 640 * 4 * rect->y;
            for (int y = rect->y; y < rect->y + rect->h; y++)
            {
                uint32 *pixel = (uint32 *) row;
                pixel += rect->x;
                for (int x = rect->x; x < rect->x + rect->w; x++)
                {
                    *pixel = (rect->color.r << 16) | (rect->color.g << 8) | (rect->color.b);
                    pixel++;
                }
                row += 640 * 4;
            }
        }
    }   
}

int32 square(int32 base)
{
    return base * base;
}

void drawCircles(World *world)
{
    uint32 entity;

    for (entity = 0; entity < ENTITY_COUNT; entity++)
    {
        if (world->mask[entity] & COMPONENT_CIRCLE)
        {
            Circle *circle = &world->circle[entity];
            int32 squareRadius = circle->radius * circle->radius;

            uint8 *row = (uint8 *) pixels;
            row += 640 * 4 * (circle->y - circle->radius);
            for (int y = circle->y - circle->radius; y < circle->y + circle->radius; y++)
            {
                uint32 *pixel = (uint32 *) row;
                pixel += circle->x - circle->radius;
                for (int x = circle->x - circle->radius; x < circle->x + circle->radius; x++)
                {
                    if (square(x - circle->x) + square(y - circle->y) <= squareRadius)
                    {
                        *pixel = (circle->color.r << 16) | (circle->color.g << 8) | (circle->color.b);
                    }
                    pixel++;
                }
                row += 640 * 4;
            }
        }
    }   
}

void initializeWorld(World *world, uint32 *lastReplicate)
{
    createFirstFractal(world);
    *lastReplicate = SDL_GetTicks();
}

void handleEvent(SDL_Event *event, bool *running, World *world, uint32 *lastReplicate)
{
    if (event->type == SDL_QUIT)
    {
        *running = false;
    }
    if (event->type == SDL_KEYDOWN && event->key.keysym.sym == SDLK_SPACE)
    {
        printf("Resetting the fractal.\n");
        deleteAllEntities(world);
        initializeWorld(world, lastReplicate);
    }
}

void wipeScreen()
{
    uint8 *row = (uint8 *) pixels;
    for (int y = 0; y < 480; y++)
    {
        uint32 *pixel = (uint32 *) row;
        for (int x = 0; x < 640; x++)
        {
            *pixel = (27 << 16) | (28 << 8) | (51);
            pixel++;
        }
        row += 640 * 4;
    }
}

int main(int argc, char **argv)
{
    if (SDL_Init(SDL_INIT_VIDEO) == 0)
    {
        SDL_Window *window;
        bool running;

        if (SDL_CreateWindowAndRenderer(640, 480, SDL_WINDOW_OPENGL, &window, &renderer) != -1)
        {
            SDL_SetWindowTitle(window, "Fractal");
            texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, 640, 480);
            pixels = mmap(0, 640 * 480 * 4, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);

            seedRNG();

            running = true;

            World world = World();

            initializeColors();

            uint32 lastReplicate = 0;
            initializeWorld(&world, &lastReplicate);

            SDL_Event event;

            while (running)
            {
                while(SDL_PollEvent(&event))
                {
                    handleEvent(&event, &running, &world, &lastReplicate);
                }

                if (SDL_GetTicks() - lastReplicate >= 100)
                {
                    printf("Replicating fractals\n");
                    replicateFractals(&world);
                    lastReplicate = SDL_GetTicks();
                }

                wipeScreen();
                drawRects(&world);
                drawCircles(&world);

                SDL_UpdateTexture(texture, 0, pixels, 640 * 4);
                SDL_RenderCopy(renderer, texture, 0, 0);
                SDL_RenderPresent(renderer);
            }

            SDL_DestroyWindow(window);
            SDL_DestroyRenderer(renderer);
            SDL_Quit();
            return 0;
        }
        else
        {
            // The window was not made successfully.
            return 1;
        }
        
    }
    else
    {
        // SDL failed to initialize.
        return 1;
    }
}