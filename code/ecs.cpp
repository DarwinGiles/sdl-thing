#include "ecs.h"

void deleteAllEntities(World *world)
{
    uint32 entity;

    for (entity = 0; entity < ENTITY_COUNT; entity++)
    {
        world->mask[entity] = COMPONENT_NONE;
    }
}

uint32 createEntity(World *world)
{
    uint32 entity;
    for (entity = 0; entity < ENTITY_COUNT; entity++)
    {
        if (world->mask[entity] == COMPONENT_NONE)
        {
            return entity;
        }
    }

    printf("ERROR! No more entities left!\n");
    return ENTITY_COUNT;
};

void destroyEntity(World *world, uint32 entity)
{
    world->mask[entity] = COMPONENT_NONE;
};