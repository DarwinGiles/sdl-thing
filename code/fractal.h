#ifndef FRACTAL_H
#define FRACTAL_H

#include "types.h"
#include "ecs.h"
#include "random.h"

#define MAX_DEPTH 4

typedef enum
{
    DIRECTION_UP = 0,
    DIRECTION_RIGHT = 1,
    DIRECTION_DOWN = 2,
    DIRECTION_LEFT = 3,
} Direction;

uint32 createFractal(World *world);
void replicateFractals(World *world);
void createFirstFractal(World *world);
void initializeColors();

#endif