#ifndef ECS_H
#define ECS_H

#include "SDL.h"

#include "types.h"

#define ENTITY_COUNT 128

typedef enum
{
    COMPONENT_NONE = 0,
    COMPONENT_Fractal = 1 << 0,
    COMPONENT_RECT = 1 << 1,
    COMPONENT_CIRCLE = 1 << 2
} Component;

typedef struct 
{
    int mask[ENTITY_COUNT];

    Fractal fractal[ENTITY_COUNT];
    Rect rect[ENTITY_COUNT];
    Circle circle[ENTITY_COUNT];
} World;

uint32 createEntity(World *world);
void deleteAllEntities(World *world);

#endif