#ifndef TYPES_H
#define TYPES_H

#define uint8 unsigned char
#define uint16 unsigned short
#define uint32 unsigned int
#define int32 int
#define global static

typedef struct
{
    uint8 r;
    uint8 g;
    uint8 b;
    uint8 a = 255;
} Color;

typedef struct
{
    int32 x;
    int32 y;
    int32 h;
    int32 w;
    Color color;
} Rect;

typedef struct
{
    int32 x;
    int32 y;
    int32 radius;
    Color color;
} Circle;

struct Fractal
{
    uint8 depth;
    int32 neighbors[4];
};

#endif