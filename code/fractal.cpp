#include "fractal.h"

Color colors[2];

Color newColor(uint8 r, uint8 g, uint8 b)
{
    Color result;
    result.r = r;
    result.g = g;
    result.b = b;
    return result;
}

bool colorsEqual(Color a, Color b)
{
    return (a.r == b.r) && (a.g == b.g) && (a.b == b.b) && (a.a == b.a);
}

void setFractalSize(World *world, uint32 entity, int32 size)
{
    if (world->mask[entity] & COMPONENT_RECT)
    {
        world->rect[entity].h = size;
        world->rect[entity].w = size;
    }
    else if (world->mask[entity] & COMPONENT_CIRCLE)
    {
        world->circle[entity].radius = size / 2;
    }
}

void setFractalColor(World *world, uint32 entity, uint8 r, uint8 g, uint8 b)
{
    Color *color;
    
    if (world->mask[entity] & COMPONENT_RECT)
    {
        color = &world->rect[entity].color;
    }
    else if (world->mask[entity] & COMPONENT_CIRCLE)
    {
        color = &world->circle[entity].color;
    }

    color->r = r;
    color->g = g;
    color->b = b;
}

void clearFractalNeighbors(World *world, uint32 entity)
{
    world->fractal[entity].neighbors[0] = -1;
    world->fractal[entity].neighbors[1] = -1;
    world->fractal[entity].neighbors[2] = -1;
    world->fractal[entity].neighbors[3] = -1;
}

uint32 createFractal(World *world)
{
    uint32 entity = createEntity(world);

    if (randBool())
    {
        world->mask[entity] = COMPONENT_Fractal | COMPONENT_RECT;
        world->rect[entity].x = 32;
        world->rect[entity].y = 32;
    }
    else
    {
        world->mask[entity] = COMPONENT_Fractal | COMPONENT_CIRCLE;
        world->circle[entity].x = 32;
        world->circle[entity].y = 32;
    }

    world->fractal[entity].depth = 1;

    setFractalSize(world, entity, 64);
    setFractalColor(world, entity, 255, 255, 255);

    clearFractalNeighbors(world, entity);

    return entity;
}

void positionAndLinkChildFractal(World *world, uint32 parent, uint32 child, Direction direction)
{
    int32 parentH;
    int32 parentW;
    
    if (world->mask[parent] & COMPONENT_RECT)
    {
        parentH = world->rect[parent].h;
        parentW = world->rect[parent].w;
    }
    else if (world->mask[parent] & COMPONENT_CIRCLE)
    {
        parentH = world->circle[parent].radius * 2;
        parentW = world->circle[parent].radius * 2;
    }

    int32 childH;
    int32 childW;
    
    if (world->mask[child] & COMPONENT_RECT)
    {
        childH = world->rect[child].h;
        childW = world->rect[child].w;
    }
    else if (world->mask[child] & COMPONENT_CIRCLE)
    {
        childH = world->circle[child].radius;
        childW = world->circle[child].radius;
    }

    int32 *childX;
    int32 *childY;

    if (world->mask[child] & COMPONENT_RECT)
    {
        childX = &world->rect[child].x;
        childY = &world->rect[child].y;
    }
    else if (world->mask[child] & COMPONENT_CIRCLE)
    {
        childX = &world->circle[child].x;
        childY = &world->circle[child].y;
    }

    switch(direction)
    {
        case DIRECTION_RIGHT:
        {
            *childX += parentW;

            if (world->mask[parent] & COMPONENT_CIRCLE)
            {
                *childX -= parentW * 0.5;

                if (world->mask[child] & COMPONENT_RECT)
                {
                    *childY -= childH * 0.5;
                }
            }
            else if (world->mask[parent] & COMPONENT_RECT)
            {
                if (world->mask[child] & COMPONENT_RECT)
                {
                    *childY += childH * 0.5;
                }
                else if (world->mask[child] & COMPONENT_CIRCLE)
                {
                    *childY += parentH * 0.5;
                }
            }

            if (world->mask[child] & COMPONENT_CIRCLE)
            {
                *childX += childW;
            }

            world->fractal[parent].neighbors[DIRECTION_RIGHT] = child;
            world->fractal[child].neighbors[DIRECTION_LEFT] = parent;
        } break;

        case DIRECTION_LEFT:
        {
            *childX -= parentW * 0.5;

            if (world->mask[parent] & COMPONENT_CIRCLE)
            {
                *childX -= parentW * 0.5;

                if (world->mask[child] & COMPONENT_RECT)
                {
                    *childY -= childH * 0.5;
                }
            }
            else if (world->mask[parent] & COMPONENT_RECT)
            {
                if (world->mask[child] & COMPONENT_RECT)
                {
                    *childY += childH * 0.5;
                }
                else if (world->mask[child] & COMPONENT_CIRCLE)
                {
                    *childY += parentH * 0.5;
                }
            }

            if (world->mask[child] & COMPONENT_CIRCLE)
            {
                *childX += childW;
            }

            world->fractal[parent].neighbors[DIRECTION_LEFT] = child;
            world->fractal[child].neighbors[DIRECTION_RIGHT] = parent;
        } break;
        case DIRECTION_DOWN:
        {
            *childY += parentH;

            if (world->mask[parent] & COMPONENT_RECT)
            {
                if (world->mask[child] & COMPONENT_RECT)
                {
                    *childX += parentW * 0.25;
                }
                else if (world->mask[child] & COMPONENT_CIRCLE)
                {
                    *childX += parentW * 0.5;
                    *childY += childH;
                }
            }
            else if (world->mask[parent] & COMPONENT_CIRCLE)
            {
                if (world->mask[child] & COMPONENT_CIRCLE)
                {
                    *childY -= parentH * 0.25;
                }
                else if (world->mask[child] & COMPONENT_RECT)
                {
                    *childY -= parentH * 0.5;
                    *childX -= parentW * 0.25;
                }
            }

            world->fractal[parent].neighbors[DIRECTION_DOWN] = child;
            world->fractal[child].neighbors[DIRECTION_UP] = parent;
        } break;
        case DIRECTION_UP:
        {
            *childY -= parentH;

            if (world->mask[parent] & COMPONENT_RECT)
            {
                if (world->mask[child] & COMPONENT_RECT)
                {
                    *childX += parentW * 0.25;
                    *childY += parentH * 0.5;
                }
                else if (world->mask[child] & COMPONENT_CIRCLE)
                {
                    *childX += parentW * 0.5;
                    *childY += parentH - childH;
                }
            }
            else if (world->mask[parent] & COMPONENT_CIRCLE)
            {
                if (world->mask[child] & COMPONENT_CIRCLE)
                {
                    *childY += parentH * 0.25;
                }
                else if (world->mask[child] & COMPONENT_RECT)
                {
                    *childX -= parentW * 0.25;
                }
            }

            world->fractal[parent].neighbors[DIRECTION_UP] = child;
            world->fractal[child].neighbors[DIRECTION_DOWN] = parent;
        } break;
    }
}

uint32 makeChildFractal(World *world, uint32 parent, Direction direction)
{
    uint32 child = createFractal(world);
    world->fractal[child].depth = world->fractal[parent].depth + 1;

    if (world->mask[parent] & COMPONENT_RECT)
    {
        setFractalSize(world, child, world->rect[parent].h * 0.5);
    }
    else if (world->mask[parent] & COMPONENT_CIRCLE)
    {
        setFractalSize(world, child, world->circle[parent].radius);
    }

    int32 parentX;
    int32 parentY;

    if (world->mask[parent] & COMPONENT_RECT)
    {
        parentX = world->rect[parent].x;
        parentY = world->rect[parent].y;
    }
    else if (world->mask[parent] & COMPONENT_CIRCLE)
    {
        parentX = world->circle[parent].x;
        parentY = world->circle[parent].y;
    }

    if (world->mask[child] & COMPONENT_RECT)
    {
        world->rect[child].x = parentX;
        world->rect[child].y = parentY;
    }
    else if (world->mask[child] & COMPONENT_CIRCLE)
    {
        world->circle[child].x = parentX;
        world->circle[child].y = parentY;
    }

    Color parentColor;

    if (world->mask[parent] & COMPONENT_RECT)
    {
        parentColor = world->rect[parent].color;
    }
    else if (world->mask[parent] & COMPONENT_CIRCLE)
    {
        parentColor = world->circle[parent].color;
    }

    if (colorsEqual(parentColor, colors[0]))
    {
        setFractalColor(world, child, colors[1].r, colors[1].g, colors[1].b);
    }
    else
    {
        setFractalColor(world, child, colors[0].r, colors[0].g, colors[0].b);
    }

    positionAndLinkChildFractal(world, parent, child, direction);

    return child;
}

void attemptReplicateFractal(World *world, uint32 entity, uint8 *newFractals, Direction dir)
{
    if (randPct() < 0.25)
    {
        world->fractal[entity].neighbors[dir] = -2;
        return;
    }

    uint32 child = makeChildFractal(world, entity, dir);
    newFractals[child] = 1;
}

void replicateFractals(World *world)
{
    uint32 entity;
    uint8 newFractals[ENTITY_COUNT];

    for (entity = 0; entity < ENTITY_COUNT; entity++)
    {
        if (world->mask[entity] & COMPONENT_Fractal)
        {
            if (world->fractal[entity].depth < MAX_DEPTH)
            {
                if (newFractals[entity] == 1)
                {
                    continue;
                }

                if (world->fractal[entity].neighbors[DIRECTION_RIGHT] == -1)
                {
                    attemptReplicateFractal(world, entity, newFractals, DIRECTION_RIGHT);
                }
                else if (world->fractal[entity].neighbors[DIRECTION_DOWN] == -1)
                {
                    attemptReplicateFractal(world, entity, newFractals, DIRECTION_DOWN);
                }
                else if (world->fractal[entity].neighbors[DIRECTION_LEFT] == -1)
                {
                    attemptReplicateFractal(world, entity, newFractals, DIRECTION_LEFT);
                }
                else if (world->fractal[entity].neighbors[DIRECTION_UP] == -1)
                {
                    attemptReplicateFractal(world, entity, newFractals, DIRECTION_UP);
                }
            }
        }
    }
}

void createFirstFractal(World *world)
{
    uint32 start = createFractal(world);

    if (world->mask[start] & COMPONENT_RECT)
    {
        world->rect[start].x = 320 - 32;
        world->rect[start].y = 240 - 32;
        world->rect[start].color = colors[0];
    }
    else
    {
        world->circle[start].x = 320;
        world->circle[start].y = 240;
        world->circle[start].color = colors[0];
    }
}

// This is stupid and makes no sense.
void initializeColors()
{
    colors[0] = newColor(40, 198, 65);
    colors[1] = newColor(211, 39, 52);
}