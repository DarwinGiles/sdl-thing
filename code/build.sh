#!/bin/bash

mkdir -p ../build
pushd ../build
c++ ../code/ecs.cpp ../code/random.cpp ../code/fractal.cpp -o fractal -g `sdl2-config --cflags --libs`
popd
