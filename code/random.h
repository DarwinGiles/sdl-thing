#ifndef RANDOM_H
#define RANDOM_H

#include </usr/include/time.h>
#include <sys/mman.h>
#include <stdlib.h>

bool randBool();
float randPct();
void seedRNG();

#endif